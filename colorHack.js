
// This is here to keep the pie chart to the same color scheme as the stacked
// bar chart. Since colors are indexed by series, and there's no productive
// hours in the pie chart, we check for the dummy 0th-series item in the pie
// chart every second and remove it if it exists.

$(document).ready(function() {
	var blue_villain;
	var pies = $('.rc-pie-series');
	var videoInterval = setInterval(function() {
		var breaded = $('.rfComponentContainer:eq(0)').find('.rfBreadCrumb').find('li:eq(1)').find('a');
		if (breaded.length) {
			$($('.rc-axis')[1]).html($($('.rc-axis')[1]).html().split("%").join("h"));
		}
		breaded.text("Breakdown");
		if (breaded)
		blue_villain = $('.pie-label-text:eq(0)');
		if (blue_villain.find('tspan').html() == '-1') {
			$('.pie-label-line:eq(0)').remove();
			$('.pie-label-text:eq(0)').remove();
		}
	}, 100);
});
